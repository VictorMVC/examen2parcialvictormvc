﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExamenListas
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String Variable;
            Variable = cbOpcion.Text;
            switch (this.cbOpcion.SelectedIndex) {

                case 0:
                    this.list.Items.Add("Carne");
                    break;

                case 1:
                    this.list.Items.Add("Pescado");
                    break;

                case 2:
                    this.list.Items.Add("Pollo");
                    break;

                case 3:
                    this.list.Items.Add("Res");
                    break;

            }
        }
    }
}
